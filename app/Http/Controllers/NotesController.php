<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;

class NotesController extends Controller
{
    private $arrayNote = [
        'error' => '',
        'result'=>[]
    ];
    public function all()
    {
        $notes = Note::all();
        foreach($notes as $note){
            $this->arrayNote['result'][] = [
                'id' => $note->id,
                'title'=> $note->title
            ];
        }
        return $this->arrayNote;
    }

    public function one($id)
    {
        $note = Note::find($id);

        if($note){
            $this->arrayNote['result'] = $note;
        }else{
            $this->arrayNote['error'] = 'Id não encontrado.';
        }

        return $this->arrayNote;
    }

    public function new(Request $request)
    {
        $title = $request->input('title');
        $body = $request->input('body');

        if($title && $body){
            $note = new Note();
            $note->title = $title;
            $note->body = $body;
            $note->save();

            $this->arrayNote['result'] = [
                'id' => $note->id,
                'title'=> $note->title,
                'body' => $note->body
            ];
        }else{
            $this->arrayNote['error'] = 'Campos não enviados';
        }
    }

    public function edit(Request $request, $id)
    {
        $title = $request->input('title');
        $body = $request->input('body');

        if($id && $title && $body){
            $note = Note::find($id);
            if($note){
                $note->title = $title;
                $note->body = $body;
                $note->save();

                $this->arrayNote['result'] = [
                    'id' => $id,
                    'title' => $title,
                    'body' => $body
                ];
            }else{
                $this->arrayNote['error'] = 'Id inexistente';
            }
        }else{
            $this->arrayNote['error'] = 'Campos não enviados';
        }

        return $this->arrayNote;
    }

    public function delete($id)
    {
        $note = Note::find($id);

        if($note){
            $note->delete();
        }else{
            $this->arrayNote['error'] = 'Id não existe';
        }

        return $this->arrayNote;
    }
}
